#version 410 core

out vec4 FragColor;

in vec2 texCoordinates;

uniform sampler2D tex0;

void main() {
	FragColor = texture(tex0 ,texCoordinates);
}


