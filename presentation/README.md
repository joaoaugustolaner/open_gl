### How to Run:

First of all, make sure that you have all the necessary libraries, like GLFW, and GLAD loaded up on your system. 
After that follow these steps:

Inside the root of the project:

 
> **[!WARNING]**
> For every directory there is a proper CMAKE so make sure you are in the correct directory (e.g.: open_gl/triangle) 



- `cmake .` for building the project and dependencies
- `make` for compiling and linking the project


### Ready to run!

Type `./app` on your **OSX or Linux** terminal, inside the root folder and voilá! 


### Controls:

When you open the `./app`, make sure to adjust the camera to see the object first if needed, but let's go to the camera controls:

You can move the camera with the following keys: 

**W - FRONT**

**S - BACK**

**A - RIGHT**

**D - LEFT**

To see the object moving you can press **B** it will follow a path based on the uncommented curve and passed configurations, more details on video.


#### Additional Info:

- Each folder follow this specific configuration, you can use it to run inside any root folder.

- Each folder is divided in chapters from [LearnOpenGl - Tutorial](https://learnopengl.com/) and subdivided in the proposed exercises.

- For a good understanding of what it is going on, I advise anyone to read the tutorial, and for the 3D part, to have a good grasp, I highly recommend
watching [Linear Algebra Series - Grant Sanderson](https://www.youtube.com/watch?v=kjBOesZCoqc&list=PL0-GT3co4r2y2YErbmuJw2L5tW4Ew2O5B)


