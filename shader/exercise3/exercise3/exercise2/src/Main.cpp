// Created by: João Augusto Tonial Laner - june 2024

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb/stb_image.h>
#include <iostream>
#include <sys/types.h>

#include "shader.h"
#include "VAO.h"
#include "VBO.h"
#include "EBO.h"


// function prototypes
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void process_input(GLFWwindow* window);

// screen configurations
const unsigned int SCREEN_WIDTH = 800;
const unsigned int SCREEN_HEIGHT = 600;

int main (int argc, char *argv[]) {
	
	// window initialization and config
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	#ifdef __APPLE__ 
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	#endif // 
	
	//window creation (throw error if not successful)
	GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Window", NULL, NULL);
	if (window == NULL){

		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;

	}
	
	// create context for the window on the current thread
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	
	// check for glad initialization
	if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)){
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// SETUP PRE PROCESS
	//
	// Vertices
	float vertices[] = {

		//first triangle   /  COLORS
		 0.0f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
	};

	GLuint indices[] = {
		0, 1, 2,
	};

	//Shaders
	
	Shader default_shaders = Shader("shaders/vert.glsl", "shaders/frag.glsl");
	
	//VAO config
	VAO Vao;
	Vao.Bind();

	//VBO setup
	VBO Vbo(vertices, sizeof(vertices));
	
	// EBO setup
	EBO Ebo(indices, sizeof(indices));
	
	
	//Link between VBO and VAO;
	Vao.LinkAttributes(Vbo, 0, 3, GL_FLOAT, 6 * sizeof(float), (void*)0);
	Vao.LinkAttributes(Vbo, 1, 3, GL_FLOAT, 6 * sizeof(float), (void*)(3* sizeof(float)));

	Vao.Unbind();
	Vbo.Unbind();
	Ebo.Unbind();

	// render loop	
	while(!glfwWindowShouldClose(window)){
		
		process_input(window);
		
		//background color
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		
		//attach shaders
		default_shaders.use();
		
		Vao.Bind();
		
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		

		//glBindVertexArray(0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	
	//Delete everything
	Vao.Delete();
	Ebo.Delete();
	Vbo.Delete();

	glfwTerminate();
	return 0;

}

void framebuffer_size_callback(GLFWwindow* window, int width, int height){
	glViewport(0, 0, width, height);
}

void process_input(GLFWwindow* window){
	
	if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

