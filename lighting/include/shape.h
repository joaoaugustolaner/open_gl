
#ifndef SHAPE_H
#define SHAPE_H

#include <GLFW/glfw3.h>


class Shape{

	
	public:

		
		static constexpr int size_vertices() {
			return sizeof(vertices);
		}

		static constexpr int size_indices() {
			return sizeof(indices);
		}
};

#endif 
