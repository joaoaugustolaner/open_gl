#pragma once

#include "curve.h"

class CatmullRom : public Curve {
	public:
		CatmullRom();
		void generateCurve(int pointsPerSegment);
};
