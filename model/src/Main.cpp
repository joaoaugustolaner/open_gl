// Created by: João Augusto Tonial Laner - june 2024

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iterator>
#include <stb/stb_image.h>
#include <iostream>
#include <string>

// libs for matrix manipulations
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader.h"
#include "camera.h"
#include "model.h"

// function prototypes
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void scroll_callback(GLFWwindow* window, double xOffSet, double yOffSet);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void process_input(GLFWwindow* window);

// screen configurations
const unsigned int SCREEN_WIDTH = 1200;
const unsigned int SCREEN_HEIGHT = 900;
//camera settings
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool firstMouse = true;
float lastX =  (float)SCREEN_WIDTH / 2.0f;
float lastY =  (float)SCREEN_HEIGHT / 2.0f;

//time calculations
float delta_time = 0.0f; // current frame - last frame 
float last_frame= 0.0f;

int ittr = 0;
// Model paths
std::string paths[] = {
	"resources/objects/Planetas/planeta.obj",
	"resources/objects/Suzanne/CuboTextured.obj",
	"resources/objects/Suzanne/SuzanneTriTextured.obj",
	"resources/objects/Suzanne/bola.obj",
	"resources/objects/Suzanne/SuzanneTri.obj",
	"resources/objects/Suzanne/SuzanneTriLowPoly.obj",
	"resources/objects/backpack/backpack.obj",
	"resources/objects/destroyer/Destroyer05.obj",
};

//MAIN
int main (int argc, char *argv[]) {
	
	// window initialization and config
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	#ifdef __APPLE__ 
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	#endif // 
	
	//window creation (throw error if not successful)
	GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Camera", NULL, NULL);
	if (window == NULL){

		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;

	}
	
	// create context for the window on the current thread
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	

	// mouse capture
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// check for glad initialization
	if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)){
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
		
	stbi_set_flip_vertically_on_load(true);

	//Enable Depth Test globally
	glEnable(GL_DEPTH_TEST);

	//Shaders
	Shader default_shaders = Shader("shaders/vert.vs", "shaders/frag.fs");
	
	//Model
	Model model = Model(paths[ittr]);	

	// render loop
    while (!glfwWindowShouldClose(window)) {
		
		model = Model(paths[ittr]);

		float current_frame = ((GLfloat)glfwGetTime());
		delta_time = current_frame - last_frame;
		last_frame = current_frame;

        // input
        process_input(window);

        // render
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		//active shader
		default_shaders.use();

		// transformations setup
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();

		default_shaders.setMat4("projection", projection);	
		default_shaders.setMat4("view", view);
	
		//render the model
	 	glm::mat4 model_matrix = glm::mat4(1.0f);
		model_matrix = glm::translate(model_matrix, glm::vec3(0.0f, 0.0f, 0.0f));
		model_matrix = glm::scale(model_matrix, glm::vec3(0.7f));		
		default_shaders.setMat4("model", model_matrix);
		
		model.Draw(default_shaders);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
	
	//clear all previews resources
    glfwTerminate();
    return 0;
}










void framebuffer_size_callback(GLFWwindow* window, int width, int height){
	glViewport(0, 0, width, height);
}

//Called whenever mouse is used
void mouse_callback(GLFWwindow* window, double xposIn, double yposIn) {
    float xpos = static_cast<float>(xposIn);
    float ypos = static_cast<float>(yposIn);

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; 

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// Called whenever scroll is used
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    camera.ProcessMouseScroll(static_cast<float>(yoffset));
}

void process_input(GLFWwindow* window){

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		camera.ProcessKeyboard(FORWARD, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		camera.ProcessKeyboard(BACKWARD, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		camera.ProcessKeyboard(LEFT, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		camera.ProcessKeyboard(RIGHT, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
		if (ittr < std::size(paths)) {
			ittr++;
		} else {
			ittr = 0;
		}
		
	}
}

