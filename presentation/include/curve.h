#pragma once


//Matrix Manipulation
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <vector> 

#include "shader.h"


class Curve {

	public:
		Curve() {}
		
		inline void setControlPoints(std::vector <glm::vec3> controlPoints) { 
			this->controlPoints = controlPoints; 
		}

		void setShader(Shader* shader);
		void generateCurve(int pointsPerSegment);
		void drawCurve(glm::vec4 color);

		int getNbCurvePoints() { 
			return curvePoints.size(); 
		}

		glm::vec3 getPointOnCurve(int i) { 
			return curvePoints[i];
		}

	protected:
		std::vector <glm::vec3> controlPoints;
		std::vector <glm::vec3> curvePoints;
		glm::mat4 visual_matrix;
		GLuint VAO;
		Shader* shader;
};
