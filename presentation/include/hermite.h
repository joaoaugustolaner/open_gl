#pragma once
#include "curve.h"

class Hermite : public Curve {

		public:
			Hermite();
			void generateCurve(int pointsPerSegment);
};
