#include "hermite.h"

Hermite::Hermite() {
	visual_matrix = glm::mat4(2, -2, 1, 1,
				  -3, 3, -2, -1,
				  0, 0, 1, 0,
				  1, 0, 0, 0		
	);
}

void Hermite::generateCurve(int pointsPerSegment) {

	float step = 1.0 / (float)pointsPerSegment;

	float time = 0;

	int nControlPoints = controlPoints.size();

	for (int i = 0; i < nControlPoints - 3; i += 3)
	{
		
		for (float t = 0.0; t <= 1.0; t += step)
		{
			glm::vec3 p;

			glm::vec4 T(t * t * t, t * t, t, 1);

			glm::vec3 P0 = controlPoints[i];
			glm::vec3 P1 = controlPoints[i + 3];
			glm::vec3 T0 = controlPoints[i + 1] - P0;
			glm::vec3 T1 = controlPoints[i + 2] - P1;

			glm::mat4x3 G(P0, P1, T0, T1);

			p = G * visual_matrix * T;  //---------

			curvePoints.push_back(p);
		}
	}


}
