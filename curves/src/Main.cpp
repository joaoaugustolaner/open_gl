// Created by: João Augusto Tonial Laner - june 2024


// imported libs by LSP or used by interfaces
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iterator>
#include <random>
#include <stb/stb_image.h>
#include <iostream>
#include <string>

// libs for matrix manipulations
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

#include "shader.h"
#include "camera.h"
#include "model.h"
#include "hermite.h"
#include "bezier.h"
#include "catmull.h"

// function prototypes
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void scroll_callback(GLFWwindow* window, double xOffSet, double yOffSet);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void process_input(GLFWwindow* window);
std::vector<glm::vec3> generateControlPointSet(int n_points);

// screen configurations
const unsigned int SCREEN_WIDTH = 1200;
const unsigned int SCREEN_HEIGHT = 900;

//curves
Bezier bez;
CatmullRom rom;
Hermite hermite;

//camera settings
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool firstMouse = true;
float lastX =  (float)SCREEN_WIDTH / 2.0f;
float lastY =  (float)SCREEN_HEIGHT / 2.0f;

//time calculations
float delta_time = 0.0f; // current frame - last frame 
float last_frame= 0.0f;

// Model iterator
int ittr = 0;
unsigned int start = 0;

// Model Paths
std::string paths[] = {
	"resources/objects/Planetas/planeta.obj",
	"resources/objects/Suzanne/CuboTextured.obj",
	"resources/objects/Suzanne/SuzanneTriTextured.obj",
	"resources/objects/Suzanne/bola.obj",
	"resources/objects/Suzanne/SuzanneTri.obj",
	"resources/objects/Suzanne/SuzanneTriLowPoly.obj",
	"resources/objects/backpack/backpack.obj",
	"resources/objects/destroyer/Destroyer05.obj",
};


//MAIN
int main (int argc, char *argv[]) {

	// window initialization and config
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


#ifdef __APPLE__ 
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif // 

	//window creation (throw error if not successful)
	GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Integralização GRAU_B - João Augusto", NULL, NULL);
	if (window == NULL){

		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;

	}

	// create context for the window on the current thread
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);


	// mouse capture
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// check for glad initialization
	if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)){
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	stbi_set_flip_vertically_on_load(true);

	//Enable Depth Test globally
	glEnable(GL_DEPTH_TEST);

	//Shaders
	Shader default_shaders = Shader("shaders/vert.vs", "shaders/frag.fs");

	default_shaders.setVec4("finalColor", glm::vec4(0, 0, 0, 1));

	std::vector<glm::vec3> controlPoints = generateControlPointSet(100);

	//Hermite setup
	hermite.setControlPoints(controlPoints);
	hermite.setShader(&default_shaders);
	hermite.generateCurve(5);


	//CatmullRom setup
	rom.setControlPoints(controlPoints);
	rom.setShader(&default_shaders);
	rom.generateCurve(10);

	//Bezier setup
	bez.setControlPoints(controlPoints);
	bez.setShader(&default_shaders);
	bez.generateCurve(100);

	int nbCurvePoints = bez.getNbCurvePoints();
	int vertexAtI = 1;
	// render loop
	while (!glfwWindowShouldClose(window)) {

		Model model = Model(paths[ittr]);

		float current_frame = ((GLfloat)glfwGetTime());
		delta_time = current_frame - last_frame;
		last_frame = current_frame;

		// input
		process_input(window);

		// render
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//rom.drawCurve(glm::vec4(1, 0, 0, 1));
		//hermite.drawCurve(glm::vec4(0, 0, 0, 1));
		bez.drawCurve(glm::vec4(0, 0, 0, 1));

		//activate shader before uniforms	
		default_shaders.use();

		//control points draw
		glDrawArrays(GL_LINE_STRIP, 0, controlPoints.size());

		// transformations setup
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();

		default_shaders.setMat4("projection", projection);	
		default_shaders.setMat4("view", view);




		//render the model
		glm::mat4 model_matrix= glm::mat4(1.0f);
		model_matrix = glm::translate(model_matrix, glm::vec3(bez.getPointOnCurve(0)));
		model_matrix = glm::scale(model_matrix, glm::vec3(0.7f));

		if (start == 1) {
			model_matrix = glm::translate(model_matrix, 
					glm::vec3(
						bez.getPointOnCurve(vertexAtI).x, 
						bez.getPointOnCurve(vertexAtI).y, 
						bez.getPointOnCurve(vertexAtI).z
						));
			if (vertexAtI < nbCurvePoints) {
				vertexAtI++;
			} else {
				vertexAtI = 0;
			}

		}

		
		default_shaders.setMat4("model", model_matrix);	

		model.Draw(default_shaders);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	//clear all previews resources
	glfwTerminate();
	return 0;
}




void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

//Called whenever mouse is used
void mouse_callback(GLFWwindow* window, double xposIn, double yposIn) {
    float xpos = static_cast<float>(xposIn);
    float ypos = static_cast<float>(yposIn);

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; 

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// Called whenever scroll is used
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    camera.ProcessMouseScroll(static_cast<float>(yoffset));
}


//keyboard input
void process_input(GLFWwindow* window){

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		camera.ProcessKeyboard(FORWARD, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		camera.ProcessKeyboard(BACKWARD, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		camera.ProcessKeyboard(LEFT, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		camera.ProcessKeyboard(RIGHT, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
		if (ittr < std::size(paths)) {
			ittr++;
		} else {
			ittr = 0;
		}
		
	}	

	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS){
		if (start == 0) {
			start = 1;
		} else {
			start = 0;
		}
	}

}


std::vector<glm::vec3> generateControlPointSet(int nPoints) {
	std::vector<glm::vec3> controlPoints;

	// Gerar números aleatórios
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> distribution(-10.0f, 10.0f); // intervalo: ]-10.0, 10.0[

	for (int i = 0; i < nPoints; i++) {
		glm::vec3 point;
		do {
			// Gerar coordenadas x e y aleatórias
			point.x = distribution(gen);
			point.y = distribution(gen);
			point.z = distribution(gen) / 2.0f;
		} while (std::find(controlPoints.begin(), controlPoints.end(), point) != controlPoints.end());

		controlPoints.push_back(point);
	}

	return controlPoints;
}



