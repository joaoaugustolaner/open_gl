### How To Run

First of all, make sure that you have all the necessary libraries, like GLFW, and GLAD loaded up on your system. 
After that follow these steps:

- `cmake .` for building the project and dependencies
- `make` for compiling and linking the project


### Ready to run!

Type `./app` on your **OSX or Linux** terminal, inside the root folder and voilá! 

#### Info:

- Each folder follow this specific configuration, you can use it to run inside any root folder.

- Each folder is divided in chapters from [LearnOpenGl - Tutorial](https://learnopengl.com/) and subdivided in the proposed exercises.

- For a good understanding of what it is going on, I advise anyone to read the tutorial and for the 3D part, to have a good grasp, I highly recommend
watching [Linear Algebra Series - Grant Sanderson](https://www.youtube.com/watch?v=kjBOesZCoqc&list=PL0-GT3co4r2y2YErbmuJw2L5tW4Ew2O5B)


