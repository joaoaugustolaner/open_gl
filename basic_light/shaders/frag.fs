#version 410 core

out vec4 FragColor;

in vec3 normal;
in vec3 fragment_position;

uniform vec3 cubeColor;
uniform vec3 lightColor;
uniform vec3 light_position;
uniform vec3 view_position;

void main() 
{
	// ambient
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;
  	
    // diffuse 
    vec3 norm = normalize(normal);
    vec3 light_direction = normalize(light_position - fragment_position);
    float diff = max(dot(norm, light_direction), 0.0);
    vec3 diffuse = diff * lightColor;
	
	// specullar
	float specular_strength = 0.3;
	vec3 view_direction = normalize(view_position - fragment_position);
	vec3 reflection_direction = reflect(-(light_direction), norm);
	float spec = pow(max(dot(view_direction, reflection_direction), 0.0), 32);
	vec3 specular = (specular_strength * spec * lightColor);

    vec3 result = (ambient + diffuse + specular) * cubeColor;
    FragColor = vec4(result, 1.0);
}


